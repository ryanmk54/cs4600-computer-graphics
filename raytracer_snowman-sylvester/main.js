/**
 * Styleguide:
 *     use tabs to indent the general source
 *     use spaces in comments
 *     try to keep trailing spaces away
 * 
 * Common objects used:
 *     Colors are represented by a 4 dimensional array [red, green, blue]
 *     Points and Vectors are represented by the Vector class in Sylvester.js
 *     Rays are represented by an object containing an anchor and direction
 */

// Setup the canvas
var canvas = document.getElementById('image-canvas');
canvas.width = 512;
canvas.height = 512;
var pixelBuffer = canvas.getContext('2d').getImageData(0, 0, canvas.width,
	canvas.height);

// The background color is the color that is displayed if no object is hit 
// while tracing
var backgroundColor = [33, 224, 202];

// Setup the camera
var camera = {
	anchor: Vector.create([8, 1.8, 30]),
	fieldOfView: 90,
	direction: Vector.create([0, 3, 0])
};

// Setup the Lights
var lights = [Vector.create([-30, -10, -20])];

// Setup the Objects
var objects = [{
		// Bottom of the snowman
		type: 'sphere',
		center: Vector.create([0, 3.5, -3]),
		color: [155, 200, 155],
		specular: 0.2,
		lambert: 0.7,
		ambient: 0.1,
		radius: 3
	}, {
		// Middle of the snowman
		type: 'sphere',
		center: Vector.create([0, 0, -3]),
		color: [155, 200, 155],
		specular: 0.2,
		lambert: 0.7,
		ambient: 0.1,
		radius: 2.1
	}, {
		// top of the snowman
		type: 'sphere',
		center: Vector.create([0, -2.6, -3]),
		color: [155, 200, 155],
		specular: 0.2,
		lambert: 0.7,
		ambient: 0.1,
		radius: 1.2
	}
, {
		// floor
		type: 'plane',
		object: Plane.XZ,
		color: getFloorColor,
		specular: 0.0,
		lambert: 0.7,
		ambient: 0.1
    }
];


/**
 * Returns the color of a tiled floor based on an x and y position.
 * Currently it just returns black to make it easier to see if it is hit
 * @returns [r, g, b]
 */
function getFloorColor(){
	return [255, 255, 255];
}


/**
 * Converts degrees to radians
 */
function toRadians(degrees){
	return Math.PI * (degrees / 2) / 180;
}


/**
 * Start raytracing
 */
function render() {
	var cameraVec = camera.direction.subtract(camera.anchor).toUnitVector();
		// Create unit vector in direction camera is looking
	var cameraVecRight = cameraVec.cross(Vector.j).toUnitVector();
		// Rotate cameraVec right 90 degrees to the right
	var cameraVecUp = cameraVecRight.cross(cameraVec).toUnitVector();
		// Rotate cameraVec right 90 degrees up

	var fieldOfViewRadians = toRadians(camera.fieldOfView);
	var heightWidthRatio = canvas.height / canvas.width;
	var halfWidth = Math.tan(fieldOfViewRadians);
	var halfHeight = heightWidthRatio * halfWidth;
	var cameraWidth = halfWidth * 2;
	var cameraHeight = halfHeight * 2;
	var pixelWidth = cameraWidth / (canvas.width - 1);
	var pixelHeight = cameraHeight / (canvas.height - 1);

	var index, color;
	var ray = {
		anchor: camera.anchor
	};
	for(var x = 0; x < canvas.width; x++){
		for(var y = 0; y < canvas.height; y++){

			var xPart = cameraVecRight.multiply(x * pixelWidth - halfWidth);
			var yPart = cameraVecUp.multiply(y * pixelHeight - halfHeight);

			ray.direction = cameraVec.add(xPart).add(yPart).toUnitVector();

			// use the vector generated to raytrace the scene, returning a color
			// as a `{x, y, z}` vector of RGB values
			color = trace(ray, 0);
			index = (x * 4) + (y * canvas.width * 4),
			pixelBuffer.data[index + 0] = color[0];
			pixelBuffer.data[index + 1] = color[1];
			pixelBuffer.data[index + 2] = color[2];
			pixelBuffer.data[index + 3] = 255;
		}
	}

	// Replace the image in the canvas with the pixel buffer
    canvas.getContext('2d').putImageData(pixelBuffer, 0, 0);
}


/**
 * Given a starting points ray.anchor and a direction ray.direction, 
 * this function returns the color that is hit by the given ray.  In order to
 * stop the ray tracer from recursing forever, a maximim of 3 rays will be 
 * traced.  Set the depth to 0 on the first call to trace.
 *
 * @param ray the ray to trace
 * @param depth - the current depth of the ray being traced
 * @return {x, y, z} the color that this ray hit in this scene
 */
function trace(ray, depth) {
	// Go no more than 3 steps deep
	if (depth > 2) return;

    var closest = intersect(ray);

    // If we don't hit anything, fill this pixel with the background color -
    // in this case, white.
    if (closest.distance === Infinity) {
        return backgroundColor;
    }

    // The `pointAtTime` is another way of saying the 'intersection point'
    // of this ray into this object. We compute this by simply taking
    // the direction of the ray and making it as long as the distance
    // returned by the intersection check.
	var intersectionPt = ray.anchor.add(ray.direction.multiply(closest.distance));

    if(closest.object.type == "sphere"){
        return getSurfaceColor(ray, closest.object, intersectionPt, 
			sphereNormal(closest.object, intersectionPt), depth);
    }
    else if(closest.object.type == "plane"){
        return getFloorColor();
    }
}


/**
 * Does an intersection test on every object in the scene with the given ray.
 * @param ray the ray to intersect with
 * @return {Object} - dist is the distance to the object it hit and object is the object it hit.
 */
function intersect(ray){
	var closest = {
		object: null,
		distance: Infinity
	}

	/* Check if each object intersects with ray.  
	 * If it does, check if the distance to that object is closer 
	 * than the closest object */
	var numObjects = objects.length;
	for (var i = 0; i < numObjects; i++) {
		var object = objects[i];
		var distToObject;

		if(object.type == "sphere"){
			dist = sphereIntersection(object, ray);
		}
		else if(object.type == "plane"){
			dist = planeIntersection(object, ray);
		}
		else{
			throw "ray intersected with unknown object."
		}

		if (dist !== undefined && dist < closest.distance) {
			closest = {
				object: object,
				distance: dist
			}
		}
	}
	return closest;
}


function planeIntersection(ray, plane){
	return;
}


/**
 * If the given ray intersects with the given sphere, returns the distance
 * from the ray's anchor to the edge of the sphere.  If the sphere doesn't
 * intersect, undefined is returned.
 */
function sphereIntersection(sphere, ray) {
	var cameraToCenter = sphere.center.subtract(ray.anchor);
		// Vector from camera to center
	var distProj = cameraToCenter.dot(ray.direction);
		// Extend the ray to the length (camera to center)
	var lengthCameraToCenter = cameraToCenter.dot(cameraToCenter);
	var discriminant = (sphere.radius * sphere.radius) - 
		lengthCameraToCenter + (distProj * distProj);

	// If discriminant is negative, the sphere hasn't been hit by the ray
	if(discriminant < 0){
		return;
	} else{
		// If the sphere has been hit by the ray, return the distance
		// from the ray's anchor to the sphere
		return distProj - Math.sqrt(discriminant);
	}
}


/**
 * Calculates the normal vector of a sphere given a sphere and
 * some point on the sphere
 * @param sphere sphere that tells us where the center is
 * @param ptOnSphere the pt on the sphere
 * @return the normal vector from the center of the sphere to the point on this sphere
 */
function sphereNormal(sphere, ptOnSphere){
	return ptOnSphere.subtract(sphere.center).toUnitVector();
}


// # Surface
//
// ![](http://farm3.staticflickr.com/2851/10524788334_f2e3903b36_b.jpg)
//
// If `trace()` determines that a ray intersected with an object, `surface`
// decides what color it acquires from the interaction.
/**
 * @return the color on a particular surface
 */
function getSurfaceColor(ray, object, pointAtTime, normal, depth) {
	return [0, 0, 0];
    //var b = object.color,
    //    c = Vector.ZERO,
    //    lambertAmount = 0;

    //// **[Lambert shading](http://en.wikipedia.org/wiki/Lambertian_reflectance)**
    //// is our pretty shading, which shows gradations from the most lit point on
    //// the object to the least.
    //if (object.lambert) {
    //    for (var i = 0; i < lights.length; i++) {
    //        var lightPoint = lights[0];
    //        // First: can we see the light? If not, this is a shadowy area
    //        // and it gets no light from the lambert shading process.
    //        if (!isLightVisible(pointAtTime, lightPoint)) continue;
    //        // Otherwise, calculate the lambertian reflectance, which
    //        // essentially is a 'diffuse' lighting system - direct light
    //        // is bright, and from there, less direct light is gradually,
    //        // beautifully, less light.
	//		var contribution = lightPoint.subtract(pointAtTime).toUnitVector().dot(normal);
    //        // sometimes this formula can return negatives, so we check:
    //        // we only want positive values for lighting.
    //        if (contribution > 0) lambertAmount += contribution;
    //    }
    //}

    //// **[Specular](https://en.wikipedia.org/wiki/Specular_reflection)** is a fancy word for 'reflective': rays that hit objects
    //// with specular surfaces bounce off and acquire the colors of other objects
    //// they bounce into.
    //if (object.specular) {
    //    // This is basically the same thing as what we did in `render()`, just
    //    // instead of looking from the viewpoint of the camera, we're looking
    //    // from a point on the surface of a shiny object, seeing what it sees
    //    // and making that part of a reflection.
    //    var reflectedRay = {
    //        anchor: pointAtTime,
    //        direction: Vector.reflectThrough(ray.vector, normal)
    //    };
    //    var reflectedColor = trace(reflectedRay, scene, ++depth);
    //    if (reflectedColor) {
    //        c = Vector.add(c, Vector.scale(reflectedColor, object.specular));
    //    }
    //}

    //// lambert should never 'blow out' the lighting of an object,
    //// even if the ray bounces between a lot of things and hits lights
    //lambertAmount = Math.min(1, lambertAmount);

    //// **Ambient** colors shine bright regardless of whether there's a light visible -
    //// a circle with a totally ambient blue color will always just be a flat blue
    //// circle.
    //return Vector.add3(c,
    //    Vector.scale(b, lambertAmount * object.lambert),
    //    Vector.scale(b, object.ambient));
}

// Check whether a light is visible from some point on the surface of something.
// Note that there might be an intersection here, which is tricky - but if it's
// tiny, it's actually an intersection with the object we're trying to decide
// the surface of. That's why we check for `> -0.005` at the end.
//
// This is the part that makes objects cast shadows on each other: from here
// we'd check to see if the area in a shadowy spot can 'see' a light, and when
// this returns `false`, we make the area shadowy.
function isLightVisible(pt, scene, light) {
    //var distObject =  intersectScene({
    //    point: pt,
    //    vector: Vector.unitVector(Vector.subtract(pt, light))
    //}, scene);
    //return distObject[0] > -0.005;
}


render();
