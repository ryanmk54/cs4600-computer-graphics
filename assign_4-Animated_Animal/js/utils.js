/**
 * @file Contains functions that make the MV.js library easier to use
 * @requires MV.js
 */


function vecToMat(vec){
    var outArr = new Array(vec.length);
    for(var index = 0; index < vec.length; index++){
        outArr[index] = new Array(vec.length);

        // Initialize the inner array to default values of 0
        for(var inArrIndex = 0; inArrIndex < outArr[index].length; inArrIndex++){
            outArr[index][inArrIndex] = 0;
        }

        // Set the relevant part of the array
        outArr[index][0] = vec[index];

    }
    outArr.matrix = true;
    return outArr;
}

function matToVec(mat){
    var vec = new Array(mat.length);
    for(var index = 0; index < mat.length; index++){
        vec[index] = mat[index][0];
    }
    return vec;
}


function scalePoints(points, x, y, z){
    var scaleMat = scalem(x, y, z);
    return transformPoints(points, scaleMat);
}


function rotatePoints(points, theta){
    var rotationMat = rotate(theta, [0, 0, 1]);
    return transformPoints(points, rotationMat);
}


function transformPoints(points, transformationMat){
    return points.map(function(point, index, pointsArray){
        return matToVec(mult(transformationMat, vecToMat(point)));
    });
}


Array.prototype.last = function(){
    return this[this.length-1];
}


Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};



function createRandomColor(){
    var createColorPart = function(){
        return Math.random();
    }
    return [createColorPart(), createColorPart(), createColorPart(), 1];
}


function createRandomColors(numColors){
    var colors = [];
    for(var i = 0; i < numColors; i++){
        colors.push(createRandomColor());
    }
    return colors;
}



function testScalePoints(){
    var v3 = vec3(1, 1, 1);
    var v3Scaled = scalePoints([v3], 1, 1);
    if(v3Scaled[0] != 1 && v3Scaled[1] != 1 && v3Scaled[2] != 1){
        var error = "scalePoints(vec3(1,1,1), 1, 1) should be vec3(1,1,1)" + 
            " but it is " + v3Scaled.toString();
        throw error;
    }
}
