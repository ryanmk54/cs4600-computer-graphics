function updateEyeX(value){
    document.getElementById("eye-x").value = value;
    document.getElementById("eye-x-val").value = value;
    eye[0] = Number(value);
}


function updateEyeY(value){
    document.getElementById("eye-y").value = value;
    document.getElementById("eye-y-val").value = value;
    eye[1] = Number(value);
}


function updateEyeZ(value){
    document.getElementById("eye-z").value = value;
    document.getElementById("eye-z-val").value = value;
    eye[2] = Number(value);
}


function updateAtX(value){
    document.getElementById("at-x").value = value;
    document.getElementById("at-x-val").value = value;
    at[0] = Number(value);
}


function updateAtY(value){
    document.getElementById("at-y").value = value;
    document.getElementById("at-y-val").value = value;
    at[1] = Number(value);
}


function updateAtZ(value){
    document.getElementById("at-z").value = value;
    document.getElementById("at-z-val").value = value;
    at[2] = Number(value);
}


function updateWalkSpeed(value){
    document.getElementById("animal-walk-speed").value = value;
    if(animal)
        animal.walkSpeed = Number(value);
}


function updateRotation(value){
    document.getElementById("animal-rotation").value = value;
    document.getElementById("animal-rotation-val").value = value;
    animal.yRotation = Number(value);
}


function resetAnimalPosition(){
    // Animal won't exist the first time this is called
    // reset should not reset the speed
    // reset should not reset the rotation
    if(animal){
        animal.reset();
    }
}


function resetEverything(){
    updateEyeX(-4);
    updateEyeY(7);
    updateEyeZ(7);
    updateAtX(10);
    updateAtY(5);
    updateAtZ(-5);
    updateWalkSpeed(0);
    resetAnimalPosition();
}


resetEverything();
