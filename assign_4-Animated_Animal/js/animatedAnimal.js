"use strict";
var program;
var canvas;
var gl;

var vertices = [];
var colors = [];
var offsets = [];

var animal;
var vertexBuffer;

var aspect;
    // Viewport aspect ratio calculated in the window onload
var modelViewMatrixLoc, projectionMatrixLoc;
var eye = vec3(0, 0, 10);
var at = vec3(0.0, 0.0, -1.0);

window.onload = function init(){
    var listener = new window.keypress.Listener();
    listener.register_combo({
        "keys" : "r",
        "on_keydown" : resetAnimalPosition
    });

    // Puts the grass in the to be buffer vars
    vertices = vertices.concat(grass.points);
    colors = colors.concat(grass.colors);
    offsets = offsets.concat(grass.points.length);

    animal = new Animal();
    vertices = vertices.concat(animal.points);
    colors = colors.concat(animal.colors);
    offsets = offsets.concat(animal.offsets);

    // Setup WebGL
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if(!gl) alert( "WebGL isn't available" );
    aspect = canvas.width/canvas.height;

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height );
    gl.clearColor(1.0, 1.0, 1.0, 1.0 );
    gl.enable(gl.DEPTH_TEST);

    //  Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram(program);

    // Load Color Data into the GPU
    var colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW);
    var vColorLoc = gl.getAttribLocation(program, "vColor");
    gl.enableVertexAttribArray(vColorLoc);
    gl.vertexAttribPointer(vColorLoc, 4, gl.FLOAT, false, 0, 0);

    // Load Vertex Data into the GPU
    vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        // set vertexBuffer as the current buffer
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
        // load data into the vertex buffer
    // Associate our shader variables with our data buffer
    var vertexPos = gl.getAttribLocation(program, "vPosition" );
        // Look up where the vertex data is
    gl.enableVertexAttribArray(vertexPos);
        // tell WebGL we want to supply data from a buffer
    gl.vertexAttribPointer(
            vertexPos, // get data from the buffer last bound with gl.bindBuffer
            4, // number of components per vertex - always 1...4
            gl.FLOAT, // type of data
            false, // normalize flag
            0, // stride - how many bytes to skip to get to the next piece of data
            0 // offset for how far into the buffer our data is
    );

    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

    render();
}


function render(){
    // Model View Matrix
    const up = vec3(0.0, 1.0, 0.0);
        // the direction that points up
    var modelViewMatrix = lookAt(eye, at , up);

    // Projection Matrix
    var near = 1;
        // positin of near clipping plane.  Put it too high and the animal will
        // be clipped
    var far = 500;
        // position of far clipping plane.  Put it too low and the animal will
        // be clipped
    var fovy = 80.0;
        // Field-of-view in Y direction angle (in degrees).
        // Assignment specs say to fix this at 80.
        // Smaller angles make the image appear bigger.
    var projectionMatrix = perspective(fovy, aspect, near, far);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        // reset buffers

    // Refresh the points

    // Set variables that are needed in the vertex and fragment shaders
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));
    gl.uniformMatrix4fv(projectionMatrixLoc, false, flatten(projectionMatrix));

    // Load Vertex Data into the GPU
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
        // load data into the vertex buffer
    // Associate our shader variables with our data buffer
    var vertexPos = gl.getAttribLocation(program, "vPosition" );
        // Look up where the vertex data is
    gl.enableVertexAttribArray(vertexPos);
        // tell WebGL we want to supply data from a buffer
    gl.vertexAttribPointer(
            vertexPos, // get data from the buffer last bound with gl.bindBuffer
            4, // number of components per vertex - always 1...4
            gl.FLOAT, // type of data
            false, // normalize flag
            0, // stride - how many bytes to skip to get to the next piece of data
            0 // offset for how far into the buffer our data is
    );

    for(var offsetIndex = 0, prevOffset = 0;
            offsetIndex < offsets.length; offsetIndex++){
        var numPoints = offsets[offsetIndex];
        gl.drawArrays(gl.TRIANGLES, prevOffset, numPoints);
        prevOffset += numPoints;
    }
    animal.walk();

    vertices = [];
    vertices = vertices.concat(grass.points);
    vertices = vertices.concat(animal.points);

    requestAnimFrame(render);
}

