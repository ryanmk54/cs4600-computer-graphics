"use strict";
var debug = false;
var program;
var canvas;
var gl;

// Bronze
//var materialAmbient =  vec4(0.2125, 0.1275, 0.054, 1.0);
//var materialDiffuse =  vec4(0.714, 0.4284, 0.18144, 1.0);
//var materialSpecular = vec4(0.393548, 0.271906, 0.166721, 1.0);
//var materialShininess = 25.6;

var vertices = [];
var texCoords = [];
var normals = [];
var colors = [];
var ambientWoLight = [];
var diffuseWoLight = [];
var specularWoLight = [];
var ambientProducts = [];
var diffuseProducts = [];
var specularProducts = [];
var phongShading = 1.0;

var vertexPos;

var animal;
var grass;
var sun;
var photoSphere;
var vertexBuffer;

var aspect;
	// Viewport aspect ratio calculated in the window onload
var modelViewMatrixLoc, projectionMatrixLoc;
var eye = vec3(0, 0, 10);
var at = vec3(0.0, 0.0, -1.0);

window.onload = function init(){
	log("Begin: init");
	registerKeyListeners();
	document.getElementById("phong-shading").checked = true;

	grass = new Grass();
	addObjectToBuffers(grass);
	animal = new Animal();
	addObjectToBuffers(animal);
	sun = new Light();
	addObjectToBuffers(sun);
	photoSphere = new PhotoSphere();
	addObjectToBuffers(photoSphere);
	resetSliders();

	calcMaterialProperties();

	// Setup and configure WebGL 
	canvas = document.getElementById( "gl-canvas" );
	gl = WebGLUtils.setupWebGL( canvas );
	if(!gl) alert( "WebGL isn't available" );
	aspect = canvas.width/canvas.height;
	gl.viewport(0, 0, canvas.width, canvas.height );
	gl.clearColor(1.0, 1.0, 1.0, 1.0 );
	gl.enable(gl.DEPTH_TEST);

	// Load shaders
	program = initShaders(gl, "vertex-shader", "fragment-shader");
	gl.useProgram(program);

	/* WebGL terminaology
	 * gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	 *  	set vertexBuffer as the current buffer
	 * gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
	 *  	load flatten(vertices) into current buffer
	 * var vertexLoc = gl.getAttribLocation(program, "vPosition" );
	 *  	get a pointer to the glAttrib(var in a shader) called vPosition
	 * gl.enableVertexAttribArray(vertexLoc);
	 *  	tell WebGL we want to supply data from a buffer
	 * gl.vertexAttribPointer(
	 *  	vertexLoc, // get data from the buffer last bound with gl.bindBuffer
	 *  	4, // number of components per vertex - always 1...4
	 *  	gl.FLOAT, // type of data
	 *  	false, // normalize flag
	 *  	0, // stride - how many bytes to skip to get to the next piece of data
	 *  	0 // offset for how far into the buffer our data is
	 * );
	 */

	// Load Vertices into the GPU
	vertexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
	var vertexLoc = gl.getAttribLocation(program, "vPosition" );
	gl.enableVertexAttribArray(vertexLoc);
	gl.vertexAttribPointer(vertexLoc, 4, gl.FLOAT, false, 0, 0);

	// Load normals into GPU
	var normalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW);
	var normalPos = gl.getUniformLocation(program, "vNormal");
	gl.enableVertexAttribArray(normalPos);
	gl.vertexAttribPointer(vertexLoc, 3, gl.FLOAT, false, 0, 0);

	// Load light properties into GPU
	var lightPosLoc = gl.getUniformLocation(program, "lightPosition");
	gl.uniform4fv(lightPosLoc, flatten(sun.position));
	var shininessLoc = gl.getUniformLocation(program, "shininess");
	gl.uniform1f(shininessLoc, 100);

	// Load material properties into GPU
	var ambientBuffer = gl.createBuffer();
	var ambientLoc = gl.getAttribLocation(program, "ambientProduct");
	gl.bindBuffer(gl.ARRAY_BUFFER, ambientBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(ambientProducts), gl.STATIC_DRAW);
	gl.enableVertexAttribArray(ambientLoc);
	gl.vertexAttribPointer(ambientLoc, 4, gl.FLOAT, false, 0, 0);
	var diffuseBuffer = gl.createBuffer();
	var diffuseLoc = gl.getAttribLocation(program, "diffuseProduct");
	gl.bindBuffer(gl.ARRAY_BUFFER, diffuseBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(diffuseProducts), gl.STATIC_DRAW);
	gl.enableVertexAttribArray(diffuseLoc);
	gl.vertexAttribPointer(diffuseLoc, 4, gl.FLOAT, false, 0, 0);
	var specularBuffer = gl.createBuffer();
	var specularLoc = gl.getAttribLocation(program, "specularProduct");
	gl.bindBuffer(gl.ARRAY_BUFFER, specularBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(specularProducts), gl.STATIC_DRAW);
	gl.enableVertexAttribArray(specularLoc);
	gl.vertexAttribPointer(specularLoc, 4, gl.FLOAT, false, 0, 0);

	var textureBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoords), gl.STATIC_DRAW);

	var vTexCoord = gl.getAttribLocation(program, "vTexCoord");
	gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(vTexCoord);

	modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
	projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

	var ryan = document.getElementById("texImage");
	configureTexture(ryan);

	refreshCanvas();
}


var modelViewMatrix;
function calcModelViewMatrix(){
    var up = vec3(0.0, 1.0, 0.0);
    modelViewMatrix = lookAt(eye, at , up);
}
var projectionMatrix;
function calcProjectionMatrix(){
    // Projection Matrix
    var near = 1;
        // positin of near clipping plane.  Put it too high and the animal will
        // be clipped
    var far = 500;
        // position of far clipping plane.  Put it too low and the animal will
        // be clipped
    var fovy = 80.0;
        // Field-of-view in Y direction angle (in degrees).
        // Assignment specs say to fix this at 80.
        // Smaller angles make the image appear bigger.
    projectionMatrix = perspective(fovy, aspect, near, far);
}


function render(){
	log("Begin: render");

	calcModelViewMatrix();
	calcProjectionMatrix();

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        // reset buffers

    // Refresh the points

    // Set modelView and projection matrix
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));
    gl.uniformMatrix4fv(projectionMatrixLoc, false, flatten(projectionMatrix));

	// Load light properties into GPU
	var lightPosLoc = gl.getUniformLocation(program, "lightPosition");
	gl.uniform4fv(lightPosLoc, flatten(sun.position));
	var shininessLoc = gl.getUniformLocation(program, "shininess");
	gl.uniform1f(shininessLoc, 100);

	// Setup phong/gourad shading
	var phongShadingLoc = gl.getUniformLocation(program, "phongShading");
	gl.uniform1f(phongShadingLoc, phongShading);

	// Set vertices
	gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
	gl.enableVertexAttribArray(gl.getAttribLocation(program, "vPosition"));
	gl.vertexAttribPointer(vertexPos, 4, gl.FLOAT, false, 0, 0);

	gl.drawArrays(gl.TRIANGLES, 0, vertices.length);
	animal.walk();

	/* For some reason these need to be done 
	 * in the same order they are done above */
	resetBuffers();
	addObjectToBuffers(grass);
	addObjectToBuffers(animal);
	addObjectToBuffers(sun);
	addObjectToBuffers(photoSphere);
}

function refreshCanvas(){
	/* Setting the fps lower makes the controls more responsive, but makes
	 * updating the image more laggy */
	var fps = 45;
	setTimeout(function(){
		render();
		window.requestAnimationFrame(refreshCanvas);
	}, 1000/fps);
}

function registerKeyListeners(){
	var listener = new window.keypress.Listener();
	listener.register_combo({
		"keys" : "r",
		"on_keydown" : resetAnimalPosition
	});
}


function log(message){
	if(debug)
		console.log(message);
}


function resetBuffers(){
	vertices = [];
	normals = [];
	ambientWoLight = [];
	diffuseWoLight = [];
	specularWoLight = [];
}


function addObjectToBuffers(object){
	vertices = vertices.concat(object.points);
	normals = normals.concat(object.normals);
	ambientWoLight = ambientWoLight.concat(object.ambientArr);
	diffuseWoLight = diffuseWoLight.concat(object.diffuseArr);
	specularWoLight = specularWoLight.concat(object.specularArr);
	texCoords = texCoords.concat(vecToTextureCoords(negateVecArr(object.points)));
}


function calcMaterialProperties(){
	ambientProducts = ambientWoLight.map(function(ambientPart, index, arr){
		return mult(sun.ambient, ambientPart);
	});
	diffuseProducts = diffuseWoLight.map(function(diffusePart, index, arr){
		return mult(sun.diffuse, diffusePart);
	});
	specularProducts = specularWoLight.map(function(specularPart, index, arr){
		return mult(sun.specular, specularPart);
	});
}


function configureTexture(image){
	var texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		// Flip the image's Y axis to match the WebGL texture coordinate space.
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
		// Upload the image into the texture.

	// Set the parameters so we can render any size image.
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR );
	gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR );
	gl.activeTexture(gl.TEXTURE0); // set which texture unit
	gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
}

