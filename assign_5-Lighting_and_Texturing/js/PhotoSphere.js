function PhotoSphere(){
	this.position = vec4(-1, 4, -12, 1);

	this.recalcPoints = function(){
		this.points = createModelSphere().points;
		this.points = scalePoints(this.points, 5, 5, 5);
		var translateMat = translate(this.position[0], this.position[1], 
				this.position[2]);
		this.points = transformPoints(this.points, translateMat);
	}.bind(this);

	this.recalcPoints();
	this.normals = this.points;
	this.nVertices = this.points.length;
	this.ambient =  vec4(1.0, 1.0, 1.0, 1.0);
	this.diffuse =  vec4(1.0, 0.0, 1.0, 1.0);
	this.specular = vec4(1.0, 1.0, 0.0, 1.0);
	this.shininess = 100;
	this.ambientArr = (new Array(this.nVertices)).fill(this.ambient);
	this.diffuseArr = (new Array(this.nVertices)).fill(this.diffuse);
	this.specularArr = (new Array(this.nVertices)).fill(this.specular);
}
