function Grass(){
	this.nVertices = 3;
	this.points = [
        vec4( 900, 0, -900, 1),
        vec4(  0, 0,   900, 1),
        vec4(-900, 0, -900, 1),
    ];
	this.normals = [
		vec4(0, 1, 0, 1),
		vec4(0, 1, 0, 1),
		vec4(0, 1, 0, 1)
	];
	this.ambient =  vec4(0.0, 0.0, 1.0, 1.0);
	this.diffuse =  vec4(0.0, 1.0, 0.0, 1.0);
	this.specular = vec4(0.0, 1.0, 1.0, 1.0);
	this.shininess = 100.0;
	this.ambientArr = (new Array(this.nVertices)).fill(this.ambient);
	this.diffuseArr = (new Array(this.nVertices)).fill(this.diffuse);
	this.specularArr = (new Array(this.nVertices)).fill(this.specular);
}



