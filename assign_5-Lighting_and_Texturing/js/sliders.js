function updateEyeX(value){
    document.getElementById("eye-x").value = value;
    document.getElementById("eye-x-val").value = value;
    eye[0] = Number(value);
}


function updateEyeY(value){
    document.getElementById("eye-y").value = value;
    document.getElementById("eye-y-val").value = value;
    eye[1] = Number(value);
}


function updateEyeZ(value){
    document.getElementById("eye-z").value = value;
    document.getElementById("eye-z-val").value = value;
    eye[2] = Number(value);
}


function updateAtX(value){
    document.getElementById("at-x").value = value;
    document.getElementById("at-x-val").value = value;
    at[0] = Number(value);
}


function updateAtY(value){
    document.getElementById("at-y").value = value;
    document.getElementById("at-y-val").value = value;
    at[1] = Number(value);
}


function updateAtZ(value){
    document.getElementById("at-z").value = value;
    document.getElementById("at-z-val").value = value;
    at[2] = Number(value);
}


function updateLightX(value){
	document.getElementById("light-x").value = value;
	document.getElementById("light-x-val").value = value;
	sun.position[0] = Number(value);
	sun.recalcPoints();
}


function updateLightY(value){
	document.getElementById("light-y").value = value;
	document.getElementById("light-y-val").value = value;
	sun.position[1] = Number(value);
	sun.recalcPoints();
}


function updateLightZ(value){
	document.getElementById("light-z").value = value;
	document.getElementById("light-z-val").value = value;
	sun.position[2] = Number(value);
	sun.recalcPoints();
}


function updateWalkSpeed(value){
    document.getElementById("animal-walk-speed").value = value;
    if(animal)
        animal.walkSpeed = Number(value);
}


function updateRotation(value){
    document.getElementById("animal-rotation").value = value;
    document.getElementById("animal-rotation-val").value = value;
    animal.yRotation = Number(value);
}


function setPhongShading(checkbox){
	if(checkbox.checked)
		phongShading = 1.0;
	else
		phongShading = 0.0;
}


function resetAnimalPosition(){
    // Animal won't exist the first time this is called
    // reset should not reset the speed
    // reset should not reset the rotation
    if(animal){
        animal.reset();
    }
}


function resetSliders(){
    updateEyeX(-3);
    updateEyeY(7);
    updateEyeZ(7);
    updateAtX(6);
    updateAtY(4);
    updateAtZ(-5);
    updateLightX(0.5);
    updateLightY(4);
    updateLightZ(0);
    updateWalkSpeed(0);
    resetAnimalPosition();
}
