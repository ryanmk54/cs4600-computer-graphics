/**
 * @file Contains functions to create an animal
 * @requires MV.js, utils.js
 */


/**
 * Creates an animal that is centered at the origin
 */
function Animal(){
    this.points = [];
    this.colors = [];
    this.normals = [];

	this.ambient  = vec4(0.0215, 0.1745, 0.0215, 1.0);
	this.diffuse  = vec4(0.07568, 0.61424, 0.07568, 1.0);
	this.specular = vec4(0.633, 0.727811, 0.633, 1.0);
	this.shininess = 25.0;

    var minRotation = 0;
    var maxRotation = 40;
    this.legRotation = 0;
    this.walkSpeed = 0;
    this.walkPos = 0;
    var time = 0;
    var matrixStack;
    this.yRotation = 0;
    var translateVec = vec4(0, 0, 0, 0);

    var lowerLegTranslate = -1.2;
    var backLegTranslate = -1.5;
    var legTranslate = 1.5;

    var torsoXScale = 2;
    var torsoYScale = 1;
    var createTorso = function(){
        var torsoPts = createModelSphere().points;
        torsoPts = scalePoints(torsoPts, torsoXScale, torsoYScale, 1);
        return torsoPts;
    }.bind(this);

    var legXScale = 0.20;
    var legYScale = 0.9;
    var createLeg = function(degreesToRotate){
        var legPts = createModelSphere().points;
        legPts = scalePoints(legPts, legXScale, legYScale, legXScale);
        if(degreesToRotate != undefined)
            legPts = rotatePoints(legPts, degreesToRotate);
        return legPts
    }

    var neckXScale = 0.4;
    var neckYScale = 1.5*legYScale;
    var createNeck = function(){
        var neckPts = createModelSphere().points;
        neckPts = scalePoints(neckPts, neckXScale, neckYScale, neckXScale);
        neckPts = rotatePoints(neckPts, 160);
        return neckPts;
    }

    var headWidth = 1;
    var headYScale = 0.4;
    var createHead = function(){
        var headPts = createModelSphere().points;
        headPts = scalePoints(headPts, headWidth, headYScale, headWidth);
        headPts = rotatePoints(headPts, 150);
        return headPts;
    }

    var eyeHeight = 0.1;
    var eyeWidth = 0.2;
    var createEye = function(){
        var eyePts = createModelSphere().points;
        eyePts = scalePoints(eyePts, eyeWidth, eyeHeight, eyeWidth);
        return eyePts;
    }

    var earHeight = 0.3;
    var earWidth = 0.2;
    var createEar = function(){
        var earPts = createModelSphere().points;
        earPts = scalePoints(earPts, earWidth, earHeight, earWidth);
        return earPts;
    }

    var noseHeight = 0.1;
    var noseWidth = 0.1;
    var createNose = function(){
        var nosePts = createModelSphere().points;
        nosePts = scalePoints(nosePts, noseWidth, noseHeight, noseWidth);
        return nosePts;
    }

    var tailHeight = 1.2;
    var tailWidth = 0.4;
    var createTail = function(){
        var tailPts = createModelSphere().points;
        tailPts = scalePoints(tailPts, tailWidth, tailHeight, tailWidth);
        tailPts = rotatePoints(tailPts, -30);
        return tailPts;
    }

    var calcItAll = function(){
        var translateMat = translate(translateVec[0], translateVec[1], translateVec[2]);
        var rotationMat = rotate(this.yRotation, vec3(0, 1, 0));
        var matrixStack= [mult(translateMat, rotationMat)];

        // Torso
        var torsoTransformation = translate(0, 5, 0);
        matrixStack.push(mult(matrixStack.last(), torsoTransformation));
        var torsoPts = createTorso();
        torsoPts = transformPoints(torsoPts, matrixStack.last());
        this.points = this.points.concat(torsoPts);

        // Upper Right Front Leg
        var urfLegTransformation = translate(legTranslate, -0.9*torsoYScale, 0.5);
        matrixStack.push(mult(matrixStack.last(), urfLegTransformation));
        var urfLegPts = createLeg(this.legRotation);
        urfLegPts = transformPoints(urfLegPts, matrixStack.last());
        this.points = this.points.concat(urfLegPts);

        // Lower Right Front Leg
        var lrfLegTransformation = translate(0, lowerLegTranslate, 0);
        matrixStack.push(mult(matrixStack.last(), lrfLegTransformation));
        var lrfLegPts = createLeg(-this.legRotation);
        lrfLegPts = transformPoints(lrfLegPts, matrixStack.last());
        this.points = this.points.concat(lrfLegPts);
        matrixStack.pop();
        matrixStack.pop();

        // Upper Left Front Leg
        var ulfLegTransformation = translate(0.7*torsoXScale, -0.9*torsoYScale, -0.5);
        matrixStack.push(mult(matrixStack.last(), ulfLegTransformation));
        var ulfLegPts = createLeg(this.legRotation);
        ulfLegPts = transformPoints(ulfLegPts, matrixStack.last());
        this.points = this.points.concat(ulfLegPts);

        // Lower Left Front Leg
        var llfLegTransformation = translate(0, lowerLegTranslate, 0);
        matrixStack.push(mult(matrixStack.last(), llfLegTransformation));
        var llfLegPts = createLeg(-this.legRotation);
        llfLegPts = transformPoints(llfLegPts, matrixStack.last());
        this.points = this.points.concat(llfLegPts);
        matrixStack.pop();
        matrixStack.pop();

        // Upper Left Back Leg
        var ulbLegTransformation = translate(-legTranslate, -0.9*torsoYScale, -0.5);
        matrixStack.push(mult(matrixStack.last(), ulbLegTransformation));
        var ulbLegPts = createLeg(this.legRotation);
        ulbLegPts = transformPoints(ulbLegPts, matrixStack.last());
        this.points = this.points.concat(ulbLegPts);

        // Lower Left Back Leg
        var llbLegTransformation = translate(0, -legYScale, 0);
        matrixStack.push(mult(matrixStack.last(), llbLegTransformation));
        var llbLegPts = createLeg(-this.legRotation);
        llbLegPts = transformPoints(llbLegPts, matrixStack.last());
        this.points = this.points.concat(llbLegPts);
        matrixStack.pop();
        matrixStack.pop();

        // Upper Right Back Leg
        var urfLegTransformation = translate(-0.6*torsoXScale, -0.9*torsoYScale, 0.5);
        matrixStack.push(mult(matrixStack.last(), urfLegTransformation));
        var urfLegPts = createLeg(this.legRotation);
        urfLegPts = transformPoints(urfLegPts, matrixStack.last());
        this.points = this.points.concat(urfLegPts);

        // Lower Right Front Leg
        var lrfLegTransformation = translate(0, lowerLegTranslate, 0);
        matrixStack.push(mult(matrixStack.last(), lrfLegTransformation));
        var lrfLegPts = createLeg(-this.legRotation);
        lrfLegPts = transformPoints(lrfLegPts, matrixStack.last());
        this.points = this.points.concat(lrfLegPts);
        matrixStack.pop();
        matrixStack.pop();

        // Neck
        var neckTransformation = translate(torsoXScale, 0.5*torsoYScale, 0);
        matrixStack.push(mult(matrixStack.last(), neckTransformation));
        var neckPts = createNeck();
        neckPts = transformPoints(neckPts, matrixStack.last());
        this.points = this.points.concat(neckPts);

        // Head
        var headTransformation = translate(1.25, 0.65, 0);
        matrixStack.push(mult(matrixStack.last(), headTransformation));
        var headPts = createHead();
        headPts = transformPoints(headPts, matrixStack.last());
        this.points = this.points.concat(headPts);

        // Right Eye
        var rEyeTransformation = translate(-0.5, 0.3, 0.7);
        matrixStack.push(mult(matrixStack.last(), rEyeTransformation));
        var rEyePts = createEye();
        rEyePts = transformPoints(rEyePts, matrixStack.last());
        this.points = this.points.concat(rEyePts);
        matrixStack.pop();

        // Left Eye
        var lEyeTransformation = translate(-0.5, 0.3, -0.7);
        matrixStack.push(mult(matrixStack.last(), lEyeTransformation));
        var lEyePts = createEye();
        lEyePts = transformPoints(lEyePts, matrixStack.last());
        this.points = this.points.concat(lEyePts);
        matrixStack.pop();

        // Right Ear
        var rEarTransformation = translate(-0.6, 0.7, 0.5);
        matrixStack.push(mult(matrixStack.last(), rEarTransformation));
        var rEarPts = createEar();
        rEarPts = transformPoints(rEarPts, matrixStack.last());
        this.points = this.points.concat(rEarPts);
        matrixStack.pop();

        // Left Ear
        var lEarTransformation = translate(-0.6, 0.7, -0.5);
        matrixStack.push(mult(matrixStack.last(), lEarTransformation));
        var lEarPts = createEar();
        lEarPts = transformPoints(lEarPts, matrixStack.last());
        this.points = this.points.concat(lEarPts);
        matrixStack.pop();

        // Right Nose
        var rNoseTransformation = translate(0.35, -0.2, 0.9);
        matrixStack.push(mult(matrixStack.last(), rNoseTransformation));
        var rNosePts = createNose();
        rNosePts = transformPoints(rNosePts, matrixStack.last());
        this.points = this.points.concat(rNosePts);
        matrixStack.pop();

        // Left Nose
        var lNoseTransformation = translate(0.35, -0.2, -0.9);
        matrixStack.push(mult(matrixStack.last(), lNoseTransformation));
        var lNosePts = createNose();
        lNosePts = transformPoints(lNosePts, matrixStack.last());
        this.points = this.points.concat(lNosePts);
        matrixStack.pop();

        // Tail
        var tailTransformation = translate(-5.3, -1.5, 0);
        matrixStack.push(mult(matrixStack.last(), tailTransformation));
        var tailPts = createTail();
        tailPts = transformPoints(tailPts, matrixStack.last());
        this.points = this.points.concat(tailPts);
        matrixStack.pop();

        matrixStack.pop();
        matrixStack.pop();
        matrixStack.pop();
		this.normals = [].concat(this.points);
			// The normals are the points b/c the animal is made of spheres
    }.bind(this);

    calcItAll();
    firstRun = false;

    this.walk = function(){
        this.points = [];
        calcItAll();

        // Move the animals legs
        time += this.walkSpeed;
        this.legRotation += this.walkSpeed*20*Math.sin(time);
        this.walkPos += this.walkSpeed;

        var rotationMat = rotate(this.yRotation, [0, 1, 0]);
        var walkSpeedAsMat  = vecToMat([this.walkSpeed, 0, 0, 0]);
        var translateMatDelta = mult(rotationMat, walkSpeedAsMat);
        translateVec = matToVec(add(vecToMat(translateVec), translateMatDelta));
    }.bind(this);

    this.resetWalk = function(){
        this.walkPos = 0;
        this.legRotation = 0;
    }.bind(this);

    this.reset = function(){
        this.resetWalk();
        translateVec = vec4(0, 0, 0, 0);
    };

	this.ambientArr = (new Array(this.points.length)).fill(this.ambient);
	this.diffuseArr = (new Array(this.points.length)).fill(this.diffuse);
	this.specularArr = (new Array(this.points.length)).fill(this.specular);
}
