function Light(){
	this.position = vec4(-1, 4, 2, 1);

	this.recalcPoints = function(){
		this.points = createModelSphere().points;
		this.points = scalePoints(this.points, 0.5, 0.5, 0.5);
		var translateMat = translate(this.position[0], this.position[1], 
				this.position[2]);
		this.points = transformPoints(this.points, translateMat);
	}.bind(this);
	this.recalcPoints();

	this.normals = [].concat(this.points);
	this.ambient =  vec4(0.2, 0.2, 0.2, 1.0);
	this.diffuse =  vec4(1.0, 1.0, 1.0, 1.0);
	this.specular = vec4(1.0, 1.0, 1.0, 1.0);
	this.shininess = 100.0;
	this.ambientArr = (new Array(this.points.length)).fill(this.ambient);
	this.diffuseArr = (new Array(this.points.length)).fill(this.diffuse);
	this.specularArr = (new Array(this.points.length)).fill(this.specular);
}
