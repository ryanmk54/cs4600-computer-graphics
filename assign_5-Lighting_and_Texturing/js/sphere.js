/**
 * @file Contains functions to create a sphere
 * @requires MV.js
 */


/**
 * Creates a sphere given four vertices of a tetrahedron and
 * the number of times to divide the tetrahedron.
 * Vertices need to be of type vec4
 * @return an object containing an array of points
 */
function createSphere(vertexA, vertexB, vertexC, vertexD, nDivisions){
	var points = [];

	var createTriangle = function(a, b, c){
		 points.push(a);
		 points.push(b);
		 points.push(c);
	}

	var divideTriangle = function(vertexA, vertexB, vertexC, nDivisions){
		if(nDivisions > 0){
			var vertexAB = normalize(mix(vertexA, vertexB, 0.5), true);
			var vertexAC = normalize(mix(vertexA, vertexC, 0.5), true);
			var vertexBC = normalize(mix(vertexB, vertexC, 0.5), true);

			divideTriangle(vertexA,  vertexAB, vertexAC, nDivisions - 1 );
			divideTriangle(vertexAB, vertexB,  vertexBC, nDivisions - 1 );
			divideTriangle(vertexBC, vertexC,  vertexAC, nDivisions - 1 );
			divideTriangle(vertexAB, vertexBC, vertexAC, nDivisions - 1 );
		}
		else{ // draw tetrahedron at end of recursion
			createTriangle(vertexA, vertexB, vertexC);
		}
	}

	divideTriangle(vertexA, vertexB, vertexC, nDivisions);
	divideTriangle(vertexD, vertexC, vertexB, nDivisions);
	divideTriangle(vertexA, vertexD, vertexB, nDivisions);
	divideTriangle(vertexA, vertexC, vertexD, nDivisions);

	return {
		points: points
	}
}


/**
 * Creates a sphere with some default values
 */
function createModelSphere(){
	var numTimesToSubdivide = 3;
	var vertexA = vec4(0.0, 0.0, -1.0, 1);
	var vertexB = vec4(0.0, 0.942809, 0.333333, 1);
	var vertexC = vec4(-0.816497, -0.471405, 0.333333, 1);
	var vertexD = vec4(0.816497, -0.471405, 0.333333, 1);
	return createSphere(
			vertexA, vertexB, vertexC, vertexD, numTimesToSubdivide
	);
}
