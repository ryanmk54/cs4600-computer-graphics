function drawFlag(){
  // Colors
  redBackground = "#EF2B2D";
  whiteLines = "#fff";
  blueLines = "#062868";


  var canvas = document.getElementById("flag-of-norway");
  var context = canvas.getContext("2d");
  context.fillStyle = redBackground;
  context.fillRect(0, 0, 1100, 800);
    // red background
  context.fillStyle = whiteLines;
  context.fillRect(300, 0, 200, 800);
    // white vertical rectangle
  context.fillRect(0, 300, 1100, 200);
    // white horizontal rectangle
  context.fillStyle = blueLines;
  context.fillRect(350, 0, 100, 800);
    // blue vertical rectangle
  context.fillRect(0, 350, 1100, 100);
    // blue horizontal rectangle
}
