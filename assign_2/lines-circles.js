"use strict";
var loggingOn = true;
var pixelHeight = 10;
var pixelWidth = 10;
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
context.fillStyle = "black";
context.fillRect(0, 0, 500, 500);

var fileReader = new FileReader();

function drawShapesInFile(files){
  if(files && files[0]){
    context.fillStyle = "black";
    context.fillRect(0, 0, 500, 500);
    var fileReader = new FileReader();
    fileReader.onload = function(evt){
      var fileContents = evt.target.result;
      var fileLines = fileContents.split("\n");
      fileLines.forEach(function(currentLine, index, fileLines){
        // Skip over blank lines
        if(currentLine != "")
          parseLine(currentLine);
      });
    }
    fileReader.readAsText(files[0]);
  }
}

function parseLine(line){
  line = line.trim();
  var words = line.split(" ");
  if(words[0].toUpperCase().match("LINE")){
    parseLineLine(words);
  }
  else if(words[0].toUpperCase().match("CIRCLE")){
    parseCircleLine(words);
  }
  else{
    throw "The first word of every line must be either LINE or CIRCLE." + 
          "It is:  " + line;
  }
}


function parseCircleLine(words){
  var centerX = parseInt(words[1]);
  var centerY = parseInt(words[2]);
  var centerPt = {x: centerX, y: centerY};
  var radius = words[3];
  var color = words[4];
  drawBressenhamCircle(centerPt, radius, color);
}


function parseLineLine(words){
  log("parsing " + words);
  var startX = parseInt(words[1]);
  var startY = parseInt(words[2]);
  var startPt = {x: startX, y: startY};
  var endX = parseInt(words[3]);
  var endY = parseInt(words[4]);
  var endPt = {x: endX, y: endY};
  var color1 = words[5];
  var color2 = words[6];
  drawLine(startPt, endPt, color1, color2);
}


function drawLine(startPt, endPt, color1, color2){
  var deltaY = endPt.y - startPt.y;
  var deltaX = endPt.x - startPt.x;
  log("deltaY: " + deltaY);
  log("deltaX: " + deltaX);

  if(deltaX == 0)
    drawVerticalLine(startPt.y, endPt.y, startPt.x, color1, color2);
  else if(deltaY == 0)
    drawHorizontalLine(startPt.x, endPt.x, startPt.y, color1, color2);
  else
    drawBressenhamLine(startPt, endPt, color1, color2);
}


function drawVerticalLine(startY, endY, x, color1, color2){
  log("Drawing a vertical line from " + startY + " to " + endY + 
      " at x point " + x); 

  if(startY > endY){
    var temp = startY;
    startY = endY;
    endY = temp;
  }

  var pointsToDraw = [];
  for(var currentY = startY; currentY <= endY; currentY++)
    pointsToDraw.push({x: x, y: currentY});
  drawPoints(pointsToDraw, color1, color2);
}



function drawHorizontalLine(startX, endX, y, color1, color2){
  log("Drawing a horizontal line from " + startX + " to " + endX + 
      " at y point " + y); 

  if(startX > endX){
    var temp = startX;
    startX = endX;
    endX = temp;
  }

  var pointsToDraw = [];
  for(var currentX = startX; currentX <= endX; currentX++)
    pointsToDraw.push({x: currentX, y: y});
  drawPoints(pointsToDraw, color1, color2);
}


function drawBressenhamLine(startPt, endPt, color1, color2){
  log("color1: " + color1);
  log("color2: " + color2);
  var deltaY = endPt.y - startPt.y;
  var deltaX = endPt.x - startPt.x;
  var slope = deltaY / deltaX;
  log("slope: " + slope);
  var flippedAboutX = false;
  var flippedAboutXY = false;
  var flippedAboutXandXY = false;

  if(startPt.x > endPt.x){
    log("swapping the points b/c startPt.x > endPt.x");
    // swap the points
    var temp = startPt;
    startPt = endPt;
    endPt = temp;

    // and the colors
    temp = color1;
    color1 = color2;
    color2 = temp;

    // Recalculate the slope
    var deltaY = endPt.y - startPt.y;
    var deltaX = endPt.x - startPt.x;
    var slope = deltaY / deltaX;
  }

  if(needsToFlipAboutX(slope)){
    flippedAboutX = true;
    startPt = flipAboutX(startPt);
    endPt = flipAboutX(endPt);
    log("coordinates after flipping about x " + 
        "(" + startPt.x + ", " + startPt.y + ") to " +
        "(" + endPt.x + ", " + endPt.y + ")");
    deltaY = endPt.y - startPt.y;
    deltaX = endPt.x - startPt.x;
    slope = deltaY/deltaX;
  }
  else if(needsToFlipAboutXY(slope)){
    flippedAboutXY = true;
    startPt = flipAboutXY(startPt);
    endPt = flipAboutXY(endPt);
    log("After flipping about xy axis: ");
    log("startPt is " + startPt.x + ", " + startPt.y);
    log("endPt is " + endPt.x + ", " + endPt.y);
    deltaY = endPt.y - startPt.y;
    deltaX = endPt.x - startPt.x;
    slope = deltaY/deltaX;
  }
  else if(needsToFlipAboutXandXY(slope)){
    flippedAboutXandXY = true;
    startPt = flipAboutX(startPt);
    endPt = flipAboutX(endPt);
    startPt = flipAboutXY(startPt);
    endPt = flipAboutXY(endPt); 

    deltaY = endPt.y - startPt.y;
    deltaX = endPt.x - startPt.x;
    slope = deltaY/deltaX;
  }

  var d = 2*deltaY - deltaX;
  var pointsToDraw = [{x: startPt.x, y: startPt.y}];

  var currentPt = startPt;
  log("start");
  while(currentPt.x < endPt.x && currentPt.y < endPt.y){
    // go NE
    if(d > 0){
      log("NE");
      currentPt.x += 1;
      currentPt.y += 1;

      var triangleNE = 2*(deltaY - deltaX);
      d = d + triangleNE;
    }
    // go E
    else{
      log("E");
      currentPt.x += 1;

      var triangleNE = 2*deltaY
      d = d + triangleNE;
    }

    pointsToDraw.push({x: currentPt.x, y: currentPt.y});
  }
  log(pointsToDraw);

  if(flippedAboutXY){
    pointsToDraw = pointsToDraw.map(function(currentPt, index, oldPts){
      var flippedPt = flipAboutXY(currentPt);
      return {x: flippedPt.x, y: flippedPt.y};
    });
  }
  else if(flippedAboutX){
    pointsToDraw = pointsToDraw.map(function(currentPt, index, oldPts){
      //log("point before flipping about x " + 
      //    "(" + currentPt.x + ", " + currentPt.y + ")");
      var flippedPt = flipAboutX(currentPt);
      log("point after flipping about x " + 
          "(" + flippedPt.x + ", " + flippedPt.y + ")");
      return {x: flippedPt.x, y: flippedPt.y};
    });
  }
  else if(flippedAboutXandXY){
    pointsToDraw = pointsToDraw.map(function(currentPt, index, oldPts){
      log(currentPt);
      var flippedPt = flipAboutXY(currentPt);
      log(flippedPt);
      flippedPt = flipAboutX(flippedPt);
      log(flippedPt);
      return {x: flippedPt.x, y: flippedPt.y};
    });
  }

  //pointsToDraw.forEach(function(currentPt, index, points){
  //  drawPixel(currentPt.x, currentPt.y, currentPt.color);
  //});
  drawPoints(pointsToDraw, color1, color2);
}

function drawPoints(points, color1, color2){
  log("color1 in drawPoints: '" + color1 + "'");
  log("color2 in drawPoints: " + color2);
  var numberPoints = points.length;
  var color1RGB = convertToRGBColor(color1);
  var color2RGB = convertToRGBColor(color2);
  log("color1RGB: " + color1RGB);
  log("color2RGB: " + color2RGB);
  var rgbDiff = color2RGB.map(function(colorPart, index, color2RGB){
    log("colorPart - color1RGB[" + index + "]");
    var rgbDiff = colorPart - color1RGB[index];
    log(colorPart + " - " + color1RGB[index] + " = " + rgbDiff);
    return rgbDiff;
  });
  log("rgbDiff: " + rgbDiff);
  var rgbDelta = rgbDiff.map(function(colorDiff, index, rgbDelta){
    return colorDiff / numberPoints;
  });
  log("rgbDelta: " + rgbDelta);
  points.forEach(function(point, index, points){
    var colorAsString = "rgb(" + Math.floor(color1RGB[0]) + "," + 
                                 Math.floor(color1RGB[1]) + "," + 
                                 Math.floor(color1RGB[2]) + ")";
    drawPixel(point.x, point.y, colorAsString);
    log("color1RGB: " + color1RGB);
    color1RGB.forEach(function(colorPart, index, color1RGBArr){
      color1RGB[index] += rgbDelta[index];
    });
  });
}



function needsToFlipAboutX(slope){
  log("needs to flip about x? " + (slope < 0 && slope >= -1));
  return slope < 0 && slope >= -1;
}


function flipAboutX(pt){
  pt.y = -pt.y;
  return pt;
}


function needsToFlipAboutXY(slope){
  return slope > 1;
}

function needsToFlipAboutXandXY(slope){
  return slope < -1;
}


function flipAboutXY(pt){
  return {x: pt.y, y: pt.x};
}


function drawBressenhamCircle(centerPt, radius, color){
  var get2ndOctantPoints = function(radius){
    var nextDirection = function(midpoint, radius){
      var nextDecision = function(midpoint, radius){
        return Math.pow(midpoint.x, 2) + 
               Math.pow(midpoint.y, 2) - 
               Math.pow(radius, 2);
      }

      if(nextDecision(midpoint, radius) < 0)
        return "East";
      else
        return "SouthEast";
    }

    var point = {x: 0, y: radius};
    var pointsToDraw = [point];
    var midpoint = {x: point.x + 1, y: point.y - 1/2};

    while(point.x < point.y){
      point = {x: point.x + 1, y: point.y};
      if(nextDirection(midpoint, radius) == "SouthEast")
        point.y -= 1;

      pointsToDraw.push(point);
      midpoint = {x: point.x + 1, y: point.y - 1/2};
    }
    return pointsToDraw;
  }

  var get3rdOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: -point.x, y: point.y};
    });
  }
  var get6thOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: -point.x, y: -point.y};
    });
  }
  var get7thOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: point.x, y: -point.y};
    });
  }
  var get1stOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: point.y, y: point.x};
    });
  }
  var get4thOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: -point.y, y: point.x};
    });
  }
  var get5thOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: -point.y, y: -point.x};
    });
  }
  var get8thOctantPoints = function(octant2Points){
    return octant2Points.map(function(point, index, array){
      return {x: point.y, y: -point.x};
    });
  }

  var octant2Points = get2ndOctantPoints(radius);
  var octant1Points = get1stOctantPoints(octant2Points);
  var octant3Points = get3rdOctantPoints(octant2Points);
  var octant4Points = get4thOctantPoints(octant2Points);
  var octant5Points = get5thOctantPoints(octant2Points);
  var octant6Points = get6thOctantPoints(octant2Points);
  var octant7Points = get7thOctantPoints(octant2Points);
  var octant8Points = get8thOctantPoints(octant2Points);
  var allPoints = octant2Points.concat(octant1Points,
                                       octant3Points,
                                       octant4Points,
                                       octant5Points,
                                       octant6Points,
                                       octant7Points,
                                       octant8Points);
  var translatePointsToCenter = function(points, centerPt){
    return points.map(function(point, index, array){
      return {x: centerPt.x + point.x, y: centerPt.y + point.y};
    });
  }
  var translatedPoints = translatePointsToCenter(allPoints, centerPt);

  translatedPoints.forEach(function(point, index, array){
    drawPixel(point.x, point.y, color);
  });
}



function drawPixel(x, y, color){
  //checkPixelInBounds(0, 0, 50, 50, x, y);
  var pt = convertPixelToCanvasPixels(x, y);
  log("color to draw pixel: " + color);
  context.fillStyle = color;
  log("context fill style: " + context.fillStyle);
  context.fillRect(pt.x, pt.y, pixelWidth, pixelHeight);
}



// Copied from StackOverflow
function convertToRGBColor(color) {
    var cvs, ctx;
    cvs = document.createElement('canvas');
    cvs.height = 1;
    cvs.width = 1;
    ctx = cvs.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, 1, 1);
    var uint8Arr = ctx.getImageData(0, 0, 1, 1).data;
    var arr = [];
    // Do this so that the values are signed, not unsigned
    uint8Arr.forEach(function(cur, index, originalArr){
      arr.push(cur);
    });
    return arr;
}


function checkPixelInBounds(smallestX, smallestY, largestX, largestY, x, y){
  if(x > largestX)
    throw "X is: " + x + ", but shouldn't be greater than " + largestX;
  if(y > largestY)
    throw "Y is: " + y + ", but shouldn't be greater than " + largestY;
  if(x < smallestX)
    throw "X is: " + x + ", but shouldn't be less than " + smallestX;
  if(y < smallestY)
    throw "Y is: " + y + ", but shouldn't be less than " + smallestY;
}

function convertPixelToCanvasPixels(x, y){
  var canvasHeight = 500;
  var convertedX = 10*x;
  var convertedY = canvasHeight - pixelHeight * y - pixelHeight;
  log("(" + x + ", " + y + ")" + 
      " converted to " + 
      "(" + convertedX + ", " + convertedY + ")");
  return {x: convertedX, y: convertedY};
}

function log(message){
  if(loggingOn)
    console.log(message);
}
