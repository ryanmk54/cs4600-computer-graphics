function testDrawPixel(){
  drawPixel(0, 0, "white");
    // bottom left
  drawPixel(10, 10, "white");
    // inside
  drawPixel(49, 49, "white");
    // top right
  drawPixel(0, 49, "white");
    // top left
  drawPixel(49, 0, "white");
    // bottom right
}


function testDrawHorizontalLine(){
  drawLine({x: 10, y: 10}, {x: 10, y: 20}, "white", "brown");
    // test out drawing a horizontal line
}


function testDrawVerticalLine(){
  drawLine({x: 35, y: 35}, {x: 5, y: 35}, "yellow", "white");
    // test out drawing a vertical line
}


function testDrawBressenhamLine(){
  //drawLine({x: 10, y: 10}, {x: 20, y: 20}, "black", "white");
  //  // slope is positive
  //drawLine({x: 0, y: 40}, {x: 40, y: 0}, "white", "black");
  //  // slope of negative 1
  //drawLine({x: 1, y: 6}, {x: 2, y: 4}, "red", "blue");
  //  // slope is negative
  //drawLine({x: 0, y: 40}, {x: 10, y: 0}, "red", "green");
  //  // steep negative slope
  //drawLine({x: 40, y: 0}, {x: 0, y: 10}, "blue", "brown");
    // steep positive slope
  drawLine({x: 10, y: 10}, {x: 1, y: 1}, "red", "blue");
    // red to blue
}


function testDrawBressenhamLinePPT(){
  drawLine({x: 5, y: 8}, {x: 9, y: 11});
}


function testDrawBressenhamCircle(){
  drawBressenhamCircle({x: 15, y: 15}, 5, "white");
}
