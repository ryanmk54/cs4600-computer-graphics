$("#speed-slider").roundSlider();
$("#rotation-slider").roundSlider();
$("#speed-slider").roundSlider({
    radius: 60,
    width: 16,
    min: 0,
    max: 50,
    editableTooltip: false,
    change: function(evt){
        setBladeSpeed(evt.value);
    }
});
$("#rotation-slider").roundSlider({
    radius: 60,
    min: 0,
    max: 360,
    width: 16,
    editableTooltip: false,
    change: function(evt){
        setRotation(evt.value);
    }
});
setXPosition(250);
setYPosition(250);
setBladeSpeed(25);
setRotation(10);
refreshCanvas();
