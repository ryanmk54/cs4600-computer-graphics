function vec3ToMat3(vec3){
    if(vec3.length != 3)
        throw "vec3 needs to be a vector of length 3";
    return mat3(vec3[0], 0, 0, vec3[1], 0, 0, vec3[2]);
}


function mat3ToVec3(mat3){
    if(mat3.length != 3)
        throw "mat3 needs to be a 3x3 matrix";
    return vec3(mat3[0][0], mat3[1][0], mat3[2][0]);
}
