"use strict"

var canvas = document.getElementById('drone-canvas');
var context = canvas.getContext('2d');

var matrixStack = [];
var droneAttr = {
    x: 500,
    y: 500,
    bladeSpeed: 0,
    degreesToRotate: 0
}

function drawPoints(points){
  context.beginPath();
  points.forEach(function(point, index, arr){
    context.lineTo(point[0], point[1]);
  });
  context.fill();
  context.closePath();
}

function transformPoints(points){
    return points.map(function(point, index, arr){
        var curMat = matrixStack[matrixStack.length-1];
        return mat3ToVec3(mult(curMat, vec3ToMat3(point)));
    });
}

function Body(){
    var bodyWidth = 100;
    var bodyHeight = 100;
    var topLeft = vec3(-bodyWidth/2, -bodyHeight/2, 1);
    this.points = [topLeft,
                   vec3(topLeft[0] + bodyWidth, topLeft[1], 1),
                   vec3(topLeft[0] + bodyWidth, topLeft[1] + bodyHeight, 1),
                   vec3(topLeft[0], topLeft[1] + bodyHeight, 1)];
    this.draw = function(){
        var curMat = mat3(1);
        curMat = mult(curMat, translate2D(droneAttr.x, droneAttr.y));
        curMat = mult(curMat, rotate2D(droneAttr.degreesToRotate));
        matrixStack.push(curMat);

        this.topArm.draw();
        this.rightArm.draw();
        this.bottomArm.draw();
        this.leftArm.draw();

        var pointsToDraw = transformPoints(this.points);
        drawPoints(pointsToDraw);
    }

    var armWidth = 10;
    var armLength = 60;
    var Arm = function(degreesToRotate, xShift, yShift){
        var topLeft = vec3(-armWidth/2, -armLength/2, 1);
        this.points = [topLeft,
                       vec3(topLeft[0]+armWidth, topLeft[1], 1),
                       vec3(topLeft[0]+armWidth, topLeft[1]+armLength, 1),
                       vec3(topLeft[0], topLeft[1]+armLength, 1)];
        this.draw = function(){
            var curMat = matrixStack[matrixStack.length - 1];
            curMat = mult(curMat, translate2D(xShift, yShift));
            curMat = mult(curMat, rotate2D(degreesToRotate));
            matrixStack.push(curMat);

            this.motor.draw();

            var pointsToDraw = transformPoints(this.points);
            drawPoints(pointsToDraw);
            matrixStack.pop();
        }

        this.motor = new function(){
            var motorWidth = 20;
            var topLeft = vec3(-motorWidth/2, -motorWidth/2, 1);
            this.curDegreesRotated = 0;
            this.points = [topLeft,
                           vec3(topLeft[0] + motorWidth, topLeft[1], 1),
                           vec3(topLeft[0] + motorWidth, topLeft[1] + motorWidth, 1),
                           vec3(topLeft[0], topLeft[1] + motorWidth, 1)];
            this.draw = function(){
                var curMat = matrixStack[matrixStack.length - 1];
                curMat = mult(curMat, translate2D(0, -armLength/2));
                curMat = mult(curMat, rotate2D(this.curDegreesRotated));
                matrixStack.push(curMat);

                this.topBlade.draw();
                this.rightBlade.draw();
                this.leftBlade.draw();
                this.bottomBlade.draw();

                var pointsToDraw = transformPoints(this.points);
                drawPoints(pointsToDraw);
                matrixStack.pop();
            }

            var Blade = function(degreesToRotate, xShift, yShift){
                var bottomRight = vec3(+motorWidth/2,0,1);
                var xDist = motorWidth;
                var yDist = 15;
                this.points = [bottomRight,
                               vec3(bottomRight[0] - motorWidth, bottomRight[1], 1),
                               vec3(bottomRight[0], bottomRight[1] - motorWidth, 1)];
                this.draw = function(){
                    var curMat = matrixStack[matrixStack.length - 1];
                    curMat = mult(curMat, translate2D(xShift, yShift));
                    curMat = mult(curMat, rotate2D(degreesToRotate));
                    matrixStack.push(curMat);

                    var pointsToDraw = transformPoints(this.points);
                    drawPoints(pointsToDraw);
                    matrixStack.pop();
                }
            }
            this.topBlade = new Blade(0, 0, -motorWidth/2);
            this.leftBlade = new Blade(90, -motorWidth/2, 0);
            this.bottomBlade = new Blade(180, 0, motorWidth/2);
            this.rightBlade = new Blade(270, motorWidth/2, 0);
        }
    }
    this.topArm = new Arm(0, 0, -bodyHeight/2 - armLength/2);
    this.rightArm = new Arm(270, bodyWidth/2 + armLength/2, 0);
    this.bottomArm = new Arm(180, 0, bodyHeight/2 + armLength/2);
    this.leftArm = new Arm(90, -bodyWidth/2 - armLength/2, 0);
}

var body = new Body();

function rotateMotors(){
    body.topArm.motor.curDegreesRotated += droneAttr.bladeSpeed;
    body.bottomArm.motor.curDegreesRotated += droneAttr.bladeSpeed;
    body.rightArm.motor.curDegreesRotated += droneAttr.bladeSpeed;
    body.leftArm.motor.curDegreesRotated += droneAttr.bladeSpeed;
}

function refreshCanvas(){
    var fps = 15;
    setTimeout(function(){
        context.fillStyle = "#02440d";
        context.fillRect(0,0,canvas.width, canvas.height);
        context.fillStyle = "#3cc4f2";
        body.draw();
        rotateMotors();
        window.requestAnimationFrame(refreshCanvas);
    }, 1000 / fps);
}
