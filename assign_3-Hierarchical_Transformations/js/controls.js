function setXPosition(xPosition){
    document.getElementById("x-val-textbox").value = xPosition;
    document.getElementById("x-slider").value = xPosition;
    droneAttr.x = xPosition;
}

function setYPosition(yPosition){
    document.getElementById("y-val-textbox").value = yPosition;
    document.getElementById("y-slider").value = (1024 - yPosition);
    droneAttr.y = yPosition;
}

function setBladeSpeed(speed){
    droneAttr.bladeSpeed = speed;
    $("#speed-slider").roundSlider("setValue", speed);
}

function setRotation(degrees){
    droneAttr.degreesToRotate = degrees;
    $("#rotation-slider").roundSlider("setValue", degrees);
}
